package com.gdghamburg.foodkart;

import android.content.Context;
import android.text.TextUtils;

import com.gdghamburg.foodkart.model.Foodkarts;
import com.gdghamburg.foodkart.model.Restaurant;
import com.gdghamburg.foodkart.util.RestaurantUtil;
import com.google.firebase.firestore.Query;

/**
 * Object for passing filters around.
 */
public class FoodkartFilters {

    private String owner = null;
    private String name = null;
    private int price = -1;
    private String sortBy = null;
    private Query.Direction sortDirection = null;

    public FoodkartFilters() {}

    public static FoodkartFilters getDefault() {
        FoodkartFilters filters = new FoodkartFilters();
        filters.setSortBy(Foodkarts.FIELD_NAME);
        filters.setSortDirection(Query.Direction.DESCENDING);

        return filters;
    }

    public boolean hasOwner() {
        return !(TextUtils.isEmpty(owner));
    }

    public boolean hasName() {
        return !(TextUtils.isEmpty(name));
    }

    public boolean hasPrice() {
        return (price > 0);
    }

    public boolean hasSortBy() {
        return !(TextUtils.isEmpty(sortBy));
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public Query.Direction getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(Query.Direction sortDirection) {
        this.sortDirection = sortDirection;
    }

    public String getSearchDescription(Context context) {
        StringBuilder desc = new StringBuilder();

        if (owner == null && name == null) {
            desc.append("<b>");
            desc.append(context.getString(R.string.all_restaurants));
            desc.append("</b>");
        }

        if (owner != null) {
            desc.append("<b>");
            desc.append(owner);
            desc.append("</b>");
        }

        if (owner != null && name != null) {
            desc.append(" in ");
        }

        if (name != null) {
            desc.append("<b>");
            desc.append(name);
            desc.append("</b>");
        }

/*        if (price > 0) {
            desc.append(" for ");
            desc.append("<b>");
            desc.append(RestaurantUtil.getPriceString(price));
            desc.append("</b>");
        }*/

        return desc.toString();
    }

    public String getOrderDescription(Context context) {
        if (Restaurant.FIELD_PRICE.equals(sortBy)) {
            return context.getString(R.string.sorted_by_price);
        } else if (Restaurant.FIELD_POPULARITY.equals(sortBy)) {
            return context.getString(R.string.sorted_by_popularity);
        } else {
            return context.getString(R.string.sorted_by_rating);
        }
    }
}

package com.gdghamburg.foodkart;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.auth.AuthUI;
import com.gdghamburg.foodkart.adapter.FoodKartAdapter;
import com.gdghamburg.foodkart.model.Foodkarts;
import com.gdghamburg.foodkart.util.RestaurantUtil;
import com.gdghamburg.foodkart.viewmodel.FoodkartActivityViewModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.WriteBatch;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FoodKartActivity extends AppCompatActivity implements
        FoodkartFilterDialogFragment.FilterListener,
        FoodKartAdapter.OnFoodkartSelectedListener {

    private static final String TAG = "MainActivity";

    private static final int RC_SIGN_IN = 9001;

    private static final int LIMIT = 50;

/*    @BindView(R.id.toolbar)
    Toolbar mToolbar;*/

    @BindView(R.id.text_current_search)
    TextView mCurrentSearchView;


    @BindView(R.id.locate_all)
    TextView mLocateAll;

    @BindView(R.id.text_current_sort_by)
    TextView mCurrentSortByView;

    @BindView(R.id.recycler_restaurants)
    RecyclerView mRestaurantsRecycler;

    @BindView(R.id.view_empty)
    ViewGroup mEmptyView;

    private FirebaseFirestore mFirestore;
    private Query mQuery;

    private FilterDialogFragment mFilterDialog;
    private FoodKartAdapter mAdapter;

    private FoodkartActivityViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foodkart);
        ButterKnife.bind(this);
        //setSupportActionBar(mToolbar);

        // View model
        mViewModel = ViewModelProviders.of(this).get(FoodkartActivityViewModel.class);

        // Enable Firestore logging
        FirebaseFirestore.setLoggingEnabled(true);

        // Firestore
        mFirestore = FirebaseFirestore.getInstance();

        // Get ${LIMIT} restaurants
        mQuery = mFirestore.collection("foodkarts")
                .orderBy("name", Query.Direction.ASCENDING)
                .limit(LIMIT);

        // RecyclerView
        mAdapter = new FoodKartAdapter(mQuery, this) {
            @Override
            protected void onDataChanged() {
                // Show/hide content if the query returns empty.
                if (getItemCount() == 0) {
                    mRestaurantsRecycler.setVisibility(View.GONE);
                    mEmptyView.setVisibility(View.VISIBLE);
                } else {
                    mRestaurantsRecycler.setVisibility(View.VISIBLE);
                    mEmptyView.setVisibility(View.GONE);
                }
            }

            @Override
            protected void onError(FirebaseFirestoreException e) {
                // Show a snackbar on errors
                Snackbar.make(findViewById(android.R.id.content),
                        "Error: check logs for info.", Snackbar.LENGTH_LONG).show();
            }
        };

        mRestaurantsRecycler.setLayoutManager(new LinearLayoutManager(this));
        mRestaurantsRecycler.setAdapter(mAdapter);

        // Filter Dialog
        mFilterDialog = new FilterDialogFragment();


    }

    @Override
    public void onStart() {
        super.onStart();

        // Start sign in if necessary
/*        if (shouldStartSignIn()) {
            startSignIn();
            return;
        }*/

        // Apply filters
        onFilter(mViewModel.getFoodkartFilters());

        // Start listening for Firestore updates
        if (mAdapter != null) {
            mAdapter.startListening();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAdapter != null) {
            mAdapter.stopListening();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add_items:
                onAddItemsClicked();
                break;
            case R.id.menu_sign_out:
                AuthUI.getInstance().signOut(this);
                startSignIn();
                break;
            case R.id.menu_delete:
                RestaurantUtil.deleteAll();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            mViewModel.setIsSigningIn(false);

            if (resultCode != RESULT_OK && shouldStartSignIn()) {
                startSignIn();
            }
        }
    }

    @OnClick(R.id.filter_bar)
    public void onFilterClicked() {
        // Show the dialog containing filter options
        mFilterDialog.show(getSupportFragmentManager(), FilterDialogFragment.TAG);
    }

    @OnClick(R.id.button_clear_filter)
    public void onClearFilterClicked() {
        mFilterDialog.resetFilters();

        onFilter(FoodkartFilters.getDefault());
    }

    @OnClick(R.id.locate_all)
    public void onLocateAllStartMap() {
        Intent intent = new Intent(this, MapActivity.class);
        ArrayList<Foodkarts> foodkarts = null;
        if(mAdapter!=null && mAdapter.getmSnapshots()!=null) {
            ArrayList<DocumentSnapshot> snapshotArrayList = mAdapter.getmSnapshots();

            foodkarts = new ArrayList();
            for (int i = 0; i <snapshotArrayList.size(); i++) {
                Foodkarts foodkart = snapshotArrayList.get(i).toObject(Foodkarts.class);
                foodkarts.add(foodkart);
            }


        }
        intent.putParcelableArrayListExtra("foodkarts", foodkarts);
        startActivity(intent);
    }

    @Override
    public void onFoodkartSelected(DocumentSnapshot restaurant) {
        onLocateAllStartMap();
    }

    @Override
    public void onFilter(FoodkartFilters filters) {
        // Construct query basic query
        Query query = mFirestore.collection("foodkarts");

        // Category (equality filter)
        if (filters.hasOwner()) {
            query = query.whereEqualTo(Foodkarts.FIELD_OWNER, filters.getOwner());
        }

        // City (equality filter)
        if (filters.hasName()) {
            query = query.whereEqualTo(Foodkarts.FIELD_NAME, filters.getName());
        }

        // Price (equality filter)
        if (filters.hasPrice()) {
            query = query.whereEqualTo(Foodkarts.FIELD_LOCATION, filters.getPrice());
        }

        // Sort by (orderBy with direction)
        if (filters.hasSortBy()) {
            query = query.orderBy(filters.getSortBy(), filters.getSortDirection());
        }

        // Limit items
        query = query.limit(LIMIT);

        // Update the query
        mAdapter.setQuery(query);

        // Set header
        mCurrentSearchView.setText(Html.fromHtml(filters.getSearchDescription(this)));
        mCurrentSortByView.setText(filters.getOrderDescription(this));

        // Save filters
        mViewModel.setFoodkartFilters(filters);
    }

    private boolean shouldStartSignIn() {
        return (!mViewModel.getIsSigningIn() && FirebaseAuth.getInstance().getCurrentUser() == null);
    }

    private void startSignIn() {
        // Sign in with FirebaseUI
        Intent intent = AuthUI.getInstance().createSignInIntentBuilder()
                .setAvailableProviders(Collections.singletonList(
                        new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build()))
                .setIsSmartLockEnabled(false)
                .build();

        startActivityForResult(intent, RC_SIGN_IN);
        mViewModel.setIsSigningIn(true);
    }

    private void onAddItemsClicked() {
        // Add a bunch of random restaurants
        WriteBatch batch = mFirestore.batch();
        for (int i = 0; i < 10; i++) {
            DocumentReference restRef = mFirestore.collection("foodkarts").document();

            // Create random restaurant / ratings
            Foodkarts randomRestaurant = RestaurantUtil.getFoodkarts(this);

            // Add restaurant
            batch.set(restRef, randomRestaurant);

        }

        batch.commit().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "Write batch succeeded.");
                } else {
                    Log.w(TAG, "write batch failed.", task.getException());
                }
            }
        });
    }
}

package com.gdghamburg.foodkart.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.IgnoreExtraProperties;

import java.util.List;

/**
 * Restaurant POJO.
 */
@IgnoreExtraProperties
public class Foodkarts implements Parcelable{

    public static final String FIELD_LOCATION = "location";
    public static final String FIELD_LOGO = "logo";
    public static final String FIELD_MENU = "menu";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_OWNER = "owner";
    public static final String FIELD_OWNER_ID = "owner_id";

    private GeoPoint location;
    private String logo;
    private List<DocumentReference> menu;
    private String name;
    private DocumentReference owner;
    private String owner_id;

    public Foodkarts() {
    }

    public Foodkarts(GeoPoint location, String logo, List menu, String name, DocumentReference owner
            , String owner_id) {
        this.location = location;
        this.logo = logo;
        this.menu = menu;
        this.name = name;
        this.owner = owner;
        this.owner_id = owner_id;
    }

    public GeoPoint getLocation() {
        return location;
    }

    public void setLocation(GeoPoint location) {
        this.location = location;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public List<DocumentReference> getMenu() {
        return menu;
    }

    public void setMenu(List<DocumentReference> menu) {
        this.menu = menu;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DocumentReference getOwner() {
        return owner;
    }

    public void setOwner(DocumentReference owner) {
        this.owner = owner;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}

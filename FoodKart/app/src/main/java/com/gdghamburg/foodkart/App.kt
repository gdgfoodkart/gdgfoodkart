package com.gdghamburg.foodkart

import android.support.multidex.MultiDexApplication
import com.google.firebase.FirebaseApp

/**
 * Created by palanisoundararajan on 26.11.17.
 */
class App: MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()

        FirebaseApp.initializeApp(this)
    }
}
package com.gdghamburg.foodkart

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast

import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity(), View.OnClickListener {

    // [START declare_auth]
    private var mAuth: FirebaseAuth? = null
    // [END declare_auth]

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        // Buttons
        email_sign_in_button.setOnClickListener(this)
        email_create_account_button.setOnClickListener(this)
        sign_out_button.setOnClickListener(this)
        verify_email_button.setOnClickListener(this)

        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance()
        // [END initialize_auth]

        navigate_to_foodkarts.setOnClickListener {
            startActivity(Intent(this, FoodKartActivity::class.java))
        }

        navigate_to_restaurant.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }

    }

    // [START on_start_check_user]
    override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val user = mAuth?.currentUser
        updateUI(user)
    }
    // [END on_start_check_user]

    private fun createAccount(email: String, password: String) {
        Log.d(TAG, "createAccount:" + email)
        if (!validateForm()) {
            return
        }

        showProgressDialog()

        // [START create_user_with_email]
        mAuth!!.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, OnCompleteListener<AuthResult> { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "createUserWithEmail:success")
                        val user = mAuth!!.currentUser
                        updateUI(user)
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "createUserWithEmail:failure", task.exception)
                        Toast.makeText(this@LoginActivity, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()
                        updateUI(null)
                    }

                    // [START_EXCLUDE]
                    hideProgressDialog()
                    // [END_EXCLUDE]
                })
        // [END create_user_with_email]
    }

    private fun signIn(email: String, password: String) {
        Log.d(TAG, "signIn:" + email)
        if (!validateForm()) {
            return
        }

        showProgressDialog()

        // [START sign_in_with_email]
        mAuth!!.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, OnCompleteListener<AuthResult> { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithEmail:success")
                        val user = mAuth!!.currentUser
                        updateUI(user)
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithEmail:failure", task.exception)
                        Toast.makeText(this@LoginActivity, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()
                        updateUI(null)
                    }

                    // [START_EXCLUDE]
                    if (!task.isSuccessful) {
                         status!!.setText(R.string.auth_failed)
                    }
                    hideProgressDialog()
                    // [END_EXCLUDE]
                })
        // [END sign_in_with_email]
    }

    private fun signOut() {
        mAuth!!.signOut()
        updateUI(null)
    }

    private fun sendEmailVerification() {
        // Disable button
        verify_email_button.setEnabled(false)

        // Send verification email
        // [START send_email_verification]
        val user = mAuth!!.currentUser
        user!!.sendEmailVerification()
                .addOnCompleteListener(this, OnCompleteListener<Void> { task ->
                    // [START_EXCLUDE]
                    // Re-enable button
                    verify_email_button.setEnabled(true)

                    if (task.isSuccessful) {
                        Toast.makeText(this@LoginActivity,
                                "Verification email sent to " + user.email!!,
                                Toast.LENGTH_SHORT).show()
                    } else {
                        Log.e(TAG, "sendEmailVerification", task.exception)
                        Toast.makeText(this@LoginActivity,
                                "Failed to send verification email.",
                                Toast.LENGTH_SHORT).show()
                    }
                    // [END_EXCLUDE]
                })
        // [END send_email_verification]
    }

    private fun validateForm(): Boolean {
        var valid = true

        val email = field_email!!.text.toString()
        if (TextUtils.isEmpty(email)) {
            field_email!!.error = "Required."
            valid = false
        } else {
            field_email!!.error = null
        }

        val password = field_password!!.text.toString()
        if (TextUtils.isEmpty(password)) {
            field_password!!.error = "Required."
            valid = false
        } else {
            field_password!!.error = null
        }

        return valid
    }

    private fun updateUI(user: FirebaseUser?) {
        hideProgressDialog()
        if (user != null) {
             status!!.text = getString(R.string.emailpassword_status_fmt,
                    user.email, user.isEmailVerified)
            detail!!.text = getString(R.string.firebase_status_fmt, user.uid)

            email_password_buttons.visibility = View.GONE
            email_password_fields.visibility = View.GONE
            signed_in_buttons.visibility = View.VISIBLE

            verify_email_button.isEnabled = !user.isEmailVerified
        } else {
             status?.setText(R.string.signed_out)
            detail?.text = null

            email_password_buttons?.visibility = View.VISIBLE
            email_password_fields?.visibility = View.VISIBLE
            signed_in_buttons?.visibility = View.GONE
        }
    }

    override fun onClick(v: View) {
        val i = v.id
        if (i == email_create_account_button.id) {
            createAccount(field_email!!.text.toString(), field_password!!.text.toString())
        } else if (i == email_sign_in_button.id) {
            signIn(field_email!!.text.toString(), field_password!!.text.toString())
        } else if (i == sign_out_button.id) {
            signOut()
        } else if (i == verify_email_button.id) {
            sendEmailVerification()
            navigateToMainActivity()
        }
    }

    fun navigateToMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
    }

    companion object {

        private val TAG = "EmailPassword"
    }
}

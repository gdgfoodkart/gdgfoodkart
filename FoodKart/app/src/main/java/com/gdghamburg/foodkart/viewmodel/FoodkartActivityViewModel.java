package com.gdghamburg.foodkart.viewmodel;

import android.arch.lifecycle.ViewModel;

import com.gdghamburg.foodkart.Filters;
import com.gdghamburg.foodkart.FoodkartFilters;
import com.gdghamburg.foodkart.MainActivity;

/**
 * ViewModel for {@link MainActivity}.
 */

public class FoodkartActivityViewModel extends ViewModel {

    private boolean mIsSigningIn;
    private FoodkartFilters mFilters;

    public FoodkartActivityViewModel() {
        mIsSigningIn = false;
        mFilters = FoodkartFilters.getDefault();
    }

    public boolean getIsSigningIn() {
        return mIsSigningIn;
    }

    public void setIsSigningIn(boolean mIsSigningIn) {
        this.mIsSigningIn = mIsSigningIn;
    }

    public FoodkartFilters getFoodkartFilters() {
        return mFilters;
    }

    public void setFoodkartFilters(FoodkartFilters mFilters) {
        this.mFilters = mFilters;
    }
}

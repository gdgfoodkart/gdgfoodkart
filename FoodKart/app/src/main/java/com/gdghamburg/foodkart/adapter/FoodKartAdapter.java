package com.gdghamburg.foodkart.adapter;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gdghamburg.foodkart.R;
import com.gdghamburg.foodkart.model.Foodkarts;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * RecyclerView adapter for a list of Restaurants.
 */
public class FoodKartAdapter extends FirestoreAdapter<FoodKartAdapter.ViewHolder> {

    public interface OnFoodkartSelectedListener {

        void onFoodkartSelected(DocumentSnapshot restaurant);

    }

    private OnFoodkartSelectedListener mListener;

    public FoodKartAdapter(Query query, OnFoodkartSelectedListener listener) {
        super(query);
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(inflater.inflate(R.layout.item_foodkart, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(getSnapshot(position), mListener);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {


/*        private GeoPoint location;
        private String logo;
        private String[] menu;
        private String name;
        private String owner;
        private String ownerId;*/

        @BindView(R.id.foodkart_item_image)
        ImageView imageView;

        @BindView(R.id.foodkart_item_name)
        TextView nameView;

        @BindView(R.id.foodkart_item_rating)
        MaterialRatingBar ratingBar;

        @BindView(R.id.foodkart_item_num_ratings)
        TextView numRatingsView;

        @BindView(R.id.foodkart_item_price)
        TextView priceView;

        @BindView(R.id.foodkart_item_category)
        TextView categoryView;

        @BindView(R.id.foodkart_item_city)
        TextView cityView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final DocumentSnapshot snapshot,
                         final OnFoodkartSelectedListener listener) {

            Foodkarts foodkarts = snapshot.toObject(Foodkarts.class);
            Resources resources = itemView.getResources();

            nameView.setText(foodkarts.getName());
            categoryView.setText(foodkarts.getLogo());

/*            // Load image
            Glide.with(imageView.getContext())
                    .load(foodkarts.getPhoto())
                    .into(imageView);


            ratingBar.setRating((float) foodkarts.getAvgRating());
            cityView.setText(foodkarts.getName());
            categoryView.setText(foodkarts.getOwner());
            numRatingsView.setText(resources.getString(R.string.fmt_num_ratings,
                    foodkarts.getNumRatings()));
            priceView.setText(RestaurantUtil.getPriceString(foodkarts));*/

            // Click listener

        itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onFoodkartSelected(snapshot);
                    }
                }
            });
        }

    }
}

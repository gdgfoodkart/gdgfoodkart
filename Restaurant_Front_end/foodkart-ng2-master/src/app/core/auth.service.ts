import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import {AngularFirestore} from "angularfire2/firestore";

interface User {
  uid: string;
  email: string;
  photoURL?: string;
  displayName?: string;
}



@Injectable()
export class AuthService {
  userId: string;
  user: any;
  authState: any = null;


  constructor(
    private afAuth: AngularFireAuth,
    private rtdb: AngularFireDatabase,
    private db: AngularFirestore,
    private router: Router) {

    /*this.afAuth.authState.subscribe(user => {

      if(!user){
        router.navigate(['/login'])
      }
      console.log(user);
      this.user = user;
    })*/

    // this.user = this.afAuth.authState
    //   .switchMap(user => {
    //     console.log(user);
    //     if (user) {
    //
    //       return this.rtdb.object<User>(`users/${user.uid}`).valueChanges()
    //     } else {
    //       return Observable.of(null)
    //     }
    //   }).do(user => {
    //     if (user) {
    //       this.userId = user.uid;
    //       this.updateOnConnect()
    //     }
    //   });

    this.afAuth.authState.subscribe((auth) => {
      console.log(auth);
      this.authState = auth;

      })


  }

  authStatus(){
    return this.afAuth.authState
  }




  googleLogin() {
    const provider = new firebase.auth.GoogleAuthProvider()
    return this.oAuthLogin(provider);
  }

  signOut() {
    this.afAuth.auth.signOut().then(() => {
      this.updateStatus('offline')
      this.router.navigate(['/']);
    });
  }

  private oAuthLogin(provider) {
    return this.afAuth.auth.signInWithPopup(provider)
      .then((credential) => {
        this.updateUserData(credential.user)
      })
  }

  /* Learn more at https://firebase.google.com/docs/database/web/offline-capabilities#section-connection-state */
  private updateOnConnect() {
    let connectedRef = firebase.database().ref('.info/connected');
    connectedRef.on('value', snap => {
      let status = snap.val() ? 'online' : 'offline'
      this.updateStatus(status)
    })
  }

  private updateStatus(status: string) {
    if (!this.userId) return
    this.rtdb.object(`users/` + this.userId).update({ status: status })
  }

  private updateUserData(user) {
    const userRef: AngularFireObject<any> = this.rtdb.object(`users/${user.uid}`);
    const data: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL
    };
    return userRef.set(data)
  }
}

import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../core/auth.service';
import {AngularFirestore} from "angularfire2/firestore";
import {AngularFireAuth} from "angularfire2/auth";


@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {

  foodkarts$: any;

  constructor(public auth: AuthService,
              private router: Router,
              private db: AngularFirestore,) {


    auth.authStatus().subscribe((auth) => {

      if (auth) {
        console.log(auth['uid']);

        this.foodkarts$ = this.db.collection('foodkarts', ref => ref.where('owner_id', '==', auth['uid'])).valueChanges().map(fkts =>
          fkts.map(fk => {
            if (fk) {
              this.router.navigate(['/']);

            } else {
              this.router.navigate(['/form']);
            }
            return fk
          })
        );

      }

    })
  }

  ngOnInit() {


  }


  signInWithGoogle(): void {
    this.auth.googleLogin().then(() => this.afterSignIn());
  }


  private afterSignIn(): void {
    this.router.navigate(['/create']);
  }

}

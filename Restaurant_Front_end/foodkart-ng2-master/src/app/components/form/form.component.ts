import {Component} from '@angular/core';
import {AngularFirestore} from 'angularfire2/firestore';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {AuthService} from '../../core/auth.service';
import {Router} from "@angular/router";
import {AngularFireAuth} from "angularfire2/auth";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-home',
  templateUrl: 'form.component.html'
})
export class FormComponent {

  foodkartsRef: any;
  user: any;
  public form: FormGroup;
  public response: string;

  constructor(private formBuilder: FormBuilder,
              private afAuth: AngularFireAuth,
              private db: AngularFirestore,
              private router: Router) {

    this.afAuth.authState.subscribe(user => {

      if (!user) {
        router.navigate(['/login'])
      }

      this.user = user;

    });

    this.foodkartsRef = db.collection('foodkarts');

    this.form = formBuilder.group({
      'name': ['', [
        Validators.required,
      ]],
      'logo': ['', [
        Validators.minLength(3),
        Validators.maxLength(10),

      ]],
      'location': ['', [
        Validators.minLength(2),
        Validators.maxLength(20),
      ]],

    })
  }

  public onSubmit() {
    console.log(this.form);
    if (!this.form.valid) {
      console.log(this.form.errors);
    } else {
      const form = this.form.value;
      const owner_id = this.user.uid;

      const newFoodkart = {
        name: form.name,
        logo: form.logo,
        owner_id
      };


      this.foodkartsRef.add(newFoodkart).then(_ => {
        this.router.navigate(['/'])
      })

    }


  }


}

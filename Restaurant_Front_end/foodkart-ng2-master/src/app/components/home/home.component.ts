import {Component} from '@angular/core';
import {AngularFirestore} from 'angularfire2/firestore';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {AuthService} from '../../core/auth.service';
import {ActivatedRoute, Router} from "@angular/router";
import {AngularFireAuth,} from "angularfire2/auth";

import {Foodkart} from '../../interfaces';
import {Validators, FormBuilder} from "@angular/forms";


@Component({
  selector: 'app-home',
  templateUrl: 'home.component.html'
})


export class HomeComponent {
  authState: any = null;
  user: any;
  ordersCollection: any;
  orders: any;
  foodkartsCollection: any;
  foodkart: any;
  products: any;
  productsCollection: any;
  form: any;
  productsRef: any;


  constructor(private db: AngularFirestore,
              private formBuilder: FormBuilder,
              private auth: AuthService,
              private afAuth: AngularFireAuth,
              private router: Router,) {

    this.productsRef = db.collection('products');



    this.form = this.formBuilder.group({
      'name': ['', [
        Validators.required,
      ]],

      'image': ['', [
        Validators.required,
      ]],
      'price': ['', [
        Validators.minLength(3),
        Validators.maxLength(10),

      ]],


    });


    this.afAuth.authState.subscribe(user => {

      if(!user){
        router.navigate(['/login'])
      }

      this.user = user;

      this.foodkartsCollection = db.collection<any>(`foodkarts`, ref => ref.where('owner_id', '==', user.uid));
      this.foodkart = this.foodkartsCollection.snapshotChanges().map(fkts =>{


        return fkts.map(fk => {
          console.log(fk.payload.doc.id);

          this.productsCollection = db.collection<any>(`products`, ref => ref.where('foodkart_id', '==', fk.payload.doc.id));
          this.products = this.productsCollection.snapshotChanges().map(pdcts =>{

            return pdcts.map(pdct => {
              console.log(pdct);
              const id = pdct.payload.doc.id;
              return {id, doc: pdct.payload.doc};
            })
          });


          const id = fk.payload.doc.id;
          return {id, doc: fk.payload.doc};
        })[0]}
      );

      this.ordersCollection = db.collection<any>(`orders`, ref => ref.where('owner_id', '==', user.uid));
      this.orders = this.ordersCollection.snapshotChanges().map(orders =>

        orders.map(order => {
          const id = order.payload.doc.id;
          return {id, doc: order.payload.doc};
        }));

    })

  }


  public onSubmit(foodkart_id) {
    console.log(foodkart_id);
    if (!this.form.valid) {
      console.log(this.form.errors);
    } else {
      const form = this.form.value;
      console.log(this.foodkart);

      const newFoodkart = {
        name: form.name,
        logo: form.image,
        price: parseInt(form.price),
        foodkart_id
      };


      this.productsRef.add(newFoodkart).then(_ => {
      })

    }


  }

  acceptOrder(order) {
    const updatedOrder = {
      state: 1
    };


    this.db.doc<any>('orders/' + order.id).update(updatedOrder);
  }

  finishOrder(order) {
    const updatedOrder = {
      state: 2
    };


    this.db.doc<any>('orders/' + order.id).update(updatedOrder);
  }


}

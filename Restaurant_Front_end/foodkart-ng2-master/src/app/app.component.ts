import { Component } from '@angular/core';
import { AuthService } from './core/auth.service';
import {Router} from "@angular/router";
import {AngularFireAuth} from "angularfire2/auth";

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ]
})
export class AppComponent  {
  name = 'AngularFire Starter';
  user: any;

  constructor(
    public auth: AuthService,
    private router: Router,
    private afAuth: AngularFireAuth,
  ) {

    this.afAuth.authState.subscribe(user => {

      if(!user){
        router.navigate(['/login'])
      }

      this.user = user;

    })
  }
}

export interface Foodkart {
  name: string;
  logo: string;
  owner: any;
  location: string[];
  menu: any[];
}
